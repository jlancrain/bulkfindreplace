﻿namespace BulkFindReplace
{
    public class ProcessResult
    {
        public bool Success { get; set; }
        public string Activity { get; set; }
    }
}
