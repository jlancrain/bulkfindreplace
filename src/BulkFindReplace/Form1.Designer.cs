﻿namespace BulkFindReplace
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSelectCsvFile = new System.Windows.Forms.Button();
            this.tbxCsvFileName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbxXmlDirectory = new System.Windows.Forms.TextBox();
            this.btnSelectXmlDir = new System.Windows.Forms.Button();
            this.btnUpdateTags = new System.Windows.Forms.Button();
            this.lblComplete = new System.Windows.Forms.Label();
            this.tboxActivity = new System.Windows.Forms.TextBox();
            this.ckbxFirstRowIsHeader = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnSelectCsvFile
            // 
            this.btnSelectCsvFile.Location = new System.Drawing.Point(44, 83);
            this.btnSelectCsvFile.Margin = new System.Windows.Forms.Padding(6);
            this.btnSelectCsvFile.Name = "btnSelectCsvFile";
            this.btnSelectCsvFile.Size = new System.Drawing.Size(102, 44);
            this.btnSelectCsvFile.TabIndex = 0;
            this.btnSelectCsvFile.Text = "Select";
            this.btnSelectCsvFile.UseVisualStyleBackColor = true;
            this.btnSelectCsvFile.Click += new System.EventHandler(this.btnSelectCsvFile_Click);
            // 
            // tbxCsvFileName
            // 
            this.tbxCsvFileName.Location = new System.Drawing.Point(158, 87);
            this.tbxCsvFileName.Margin = new System.Windows.Forms.Padding(6);
            this.tbxCsvFileName.Name = "tbxCsvFileName";
            this.tbxCsvFileName.Size = new System.Drawing.Size(756, 31);
            this.tbxCsvFileName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(152, 38);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(490, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "CSV input file (formatted First 2 columns old,new )";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(152, 202);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(263, 25);
            this.label2.TabIndex = 5;
            this.label2.Text = "Directory of files to update";
            // 
            // tbxXmlDirectory
            // 
            this.tbxXmlDirectory.Location = new System.Drawing.Point(158, 252);
            this.tbxXmlDirectory.Margin = new System.Windows.Forms.Padding(6);
            this.tbxXmlDirectory.Name = "tbxXmlDirectory";
            this.tbxXmlDirectory.Size = new System.Drawing.Size(756, 31);
            this.tbxXmlDirectory.TabIndex = 4;
            // 
            // btnSelectXmlDir
            // 
            this.btnSelectXmlDir.Location = new System.Drawing.Point(44, 248);
            this.btnSelectXmlDir.Margin = new System.Windows.Forms.Padding(6);
            this.btnSelectXmlDir.Name = "btnSelectXmlDir";
            this.btnSelectXmlDir.Size = new System.Drawing.Size(102, 44);
            this.btnSelectXmlDir.TabIndex = 3;
            this.btnSelectXmlDir.Text = "Select";
            this.btnSelectXmlDir.UseVisualStyleBackColor = true;
            this.btnSelectXmlDir.Click += new System.EventHandler(this.btnSelectXmlDir_Click);
            // 
            // btnUpdateTags
            // 
            this.btnUpdateTags.Location = new System.Drawing.Point(44, 333);
            this.btnUpdateTags.Margin = new System.Windows.Forms.Padding(6);
            this.btnUpdateTags.Name = "btnUpdateTags";
            this.btnUpdateTags.Size = new System.Drawing.Size(472, 94);
            this.btnUpdateTags.TabIndex = 6;
            this.btnUpdateTags.Text = "Update Files";
            this.btnUpdateTags.UseVisualStyleBackColor = true;
            this.btnUpdateTags.Click += new System.EventHandler(this.btnUpdateTags_Click);
            // 
            // lblComplete
            // 
            this.lblComplete.AutoSize = true;
            this.lblComplete.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComplete.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lblComplete.Location = new System.Drawing.Point(634, 354);
            this.lblComplete.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblComplete.Name = "lblComplete";
            this.lblComplete.Size = new System.Drawing.Size(267, 63);
            this.lblComplete.TabIndex = 7;
            this.lblComplete.Text = "Complete";
            this.lblComplete.Visible = false;
            // 
            // tboxActivity
            // 
            this.tboxActivity.BackColor = System.Drawing.SystemColors.Control;
            this.tboxActivity.Location = new System.Drawing.Point(44, 493);
            this.tboxActivity.Margin = new System.Windows.Forms.Padding(6);
            this.tboxActivity.Multiline = true;
            this.tboxActivity.Name = "tboxActivity";
            this.tboxActivity.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tboxActivity.Size = new System.Drawing.Size(888, 362);
            this.tboxActivity.TabIndex = 8;
            // 
            // ckbxFirstRowIsHeader
            // 
            this.ckbxFirstRowIsHeader.AutoSize = true;
            this.ckbxFirstRowIsHeader.Location = new System.Drawing.Point(158, 127);
            this.ckbxFirstRowIsHeader.Name = "ckbxFirstRowIsHeader";
            this.ckbxFirstRowIsHeader.Size = new System.Drawing.Size(232, 29);
            this.ckbxFirstRowIsHeader.TabIndex = 9;
            this.ckbxFirstRowIsHeader.Text = "First Row Is Header";
            this.ckbxFirstRowIsHeader.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(39, 462);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 25);
            this.label3.TabIndex = 10;
            this.label3.Text = "Activity";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(998, 891);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ckbxFirstRowIsHeader);
            this.Controls.Add(this.tboxActivity);
            this.Controls.Add(this.lblComplete);
            this.Controls.Add(this.btnUpdateTags);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbxXmlDirectory);
            this.Controls.Add(this.btnSelectXmlDir);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbxCsvFileName);
            this.Controls.Add(this.btnSelectCsvFile);
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "Form1";
            this.Text = "Bulk Find Replace V1.1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSelectCsvFile;
        private System.Windows.Forms.TextBox tbxCsvFileName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbxXmlDirectory;
        private System.Windows.Forms.Button btnSelectXmlDir;
        private System.Windows.Forms.Button btnUpdateTags;
        private System.Windows.Forms.Label lblComplete;
        private System.Windows.Forms.TextBox tboxActivity;
        private System.Windows.Forms.CheckBox ckbxFirstRowIsHeader;
        private System.Windows.Forms.Label label3;
    }
}

