﻿using System;
using System.Windows.Forms;

namespace BulkFindReplace
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            HideMessages();
        }

        private void HideMessages()
        {
            this.lblComplete.Visible = false;
        }

        private void btnSelectCsvFile_Click(object sender, EventArgs e)
        {
            HideMessages();
            var fileSelect = new OpenFileDialog();
            fileSelect.Filter = "CSV Files (*.csv)|*.csv";
            var result = fileSelect.ShowDialog();
            if (result == DialogResult.OK)
            {
                this.tbxCsvFileName.Text = fileSelect.FileName;
            }
        }

        private void btnSelectXmlDir_Click(object sender, EventArgs e)
        {
            HideMessages();
            var fileSelect = new FolderBrowserDialog();
            var result = fileSelect.ShowDialog();
            if (result == DialogResult.OK)
            {
                this.tbxXmlDirectory.Text = fileSelect.SelectedPath;
            }
        }

        private void btnUpdateTags_Click(object sender, EventArgs e)
        {
            HideMessages();
            if (!string.IsNullOrEmpty(this.tbxCsvFileName.Text) || !string.IsNullOrEmpty(this.tbxXmlDirectory.Text))
            {
                var service = new UpdaterService();
                var response = service.ProcessFiles(this.tbxCsvFileName.Text, this.tbxXmlDirectory.Text, this.ckbxFirstRowIsHeader.Checked);
                this.tboxActivity.Text = response.Activity;

                if (response.Success)
                {
                    this.lblComplete.Visible = true;
                }
                else
                {
                    this.lblComplete.Visible = false;
                }
            }
        }
    }
}
