﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BulkFindReplace
{
    public class UpdaterService
    {
        public ProcessResult ProcessFiles(string csvFile, string replaceDir, bool FirstRowIsHeader)
        {
            var startTime = DateTime.Now;
            var result = new ProcessResult();
            result.Success = false;
            StringBuilder Activity = new StringBuilder();

            try
            {
                var rawData = GetFileContent(csvFile);
                var tagList = ConvertCsvToObject(rawData, FirstRowIsHeader).ToList();
                Activity.AppendLine($"Open CSV file... {tagList.Count} strings to replace");

                var fileList = GetFilesInDir(replaceDir).ToList();
                Activity.AppendLine($"Searching Directory... {fileList.Count} File(s) found");

                var destinationDir = $"{replaceDir}{DateTime.Today.Date:yyyyMMdd}";
                CreateDirIfNotExist(destinationDir);
                Activity.AppendLine($"Created Directory... {destinationDir}");

                var loopResult = Parallel.ForEach(fileList, fileInfo =>
                {
                    var fileData = GetFileText(fileInfo.FullName);

                    foreach (var tag in tagList)
                    {
                        fileData = fileData.Replace(tag.OldValue, tag.NewValue);
                    }

                    CreateTextFile(destinationDir, fileInfo.Name, fileData);
                    Activity.AppendLine($"Processing {fileInfo.Name} completed");
                });

                Activity.AppendLine($"Completed in {(DateTime.Now - startTime).TotalSeconds} seconds");
                if (loopResult.IsCompleted)
                {
                    result.Success = true;
                }
                
            }
            catch (Exception e)
            {
                Activity.AppendLine($"Error: {e.Message}");
                result.Success = false;
            }

            result.Activity = Activity.ToString();
            return result;
        }

        private IEnumerable<TagData> ConvertCsvToObject(IEnumerable<string> csv, bool HasHeaderRow)
        {
            var dataList = new List<TagData>();
            if (HasHeaderRow)
            {
                csv = csv.Skip(1);
            }

            foreach (var value in csv)
            {
                var values = value.Split(',');
                if (values.Length >= 2)
                {
                    dataList.Add(new TagData { OldValue = values[0], NewValue = values[1] });
                }
            }

            return dataList;
        }

        private IEnumerable<string> GetFileContent(string fileFullName)
        {
            var fileContnet = new List<string>();
            var fileData = File.ReadAllLines(fileFullName);
            fileContnet.AddRange(fileData);
            return fileContnet;
        }


        private string GetFileText(string fileFullName)
        {
            var fileData = File.ReadAllText(fileFullName);
            return fileData;
        }

        private void CreateTextFile(string path, string fileName, string data)
        {
            var dataList = new List<string>();
            using (var reader = new StringReader(data))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    dataList.Add(line);
                }
            }

            CreateTextFile(path, fileName, dataList);
        }

        private void CreateTextFile(string path, string fileName, List<string> dataList)
        {
            using (var fileStream = new FileStream(Path.Combine(path, fileName), FileMode.Create))
            {
                using (var write = new StreamWriter(fileStream))
                {
                    foreach (var data in dataList)
                    {
                        if (data != dataList.LastOrDefault())
                        {
                            write.WriteLine(data);
                        }
                        else
                        {
                            write.Write(data);
                        }
                    }
                }
            }
        }

        private void CreateDirIfNotExist(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        private IEnumerable<FileInfo> GetFilesInDir(string path)
        {
            var directoryInfo = new DirectoryInfo(path);
            var fileInfo = directoryInfo.GetFiles().ToList();
            return fileInfo;

        }
    }
}
