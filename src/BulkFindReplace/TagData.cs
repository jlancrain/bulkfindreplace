﻿namespace BulkFindReplace
{
    public class TagData
    {
        public string OldValue { get; set; }
        public string NewValue { get; set; }    
    }
}
